#version 450 core

uniform struct Material {
    vec4 AlbedoColor;

    int UseAlbedoTex;
    sampler2D AlbedoTex;

} uMaterial;

in VS_OUT {
    vec2 TexCoords;
    vec3 Normal;
} fs_in;

out vec4 fragColor;

void main() {
    vec4 outColor = uMaterial.AlbedoColor;

    if(uMaterial.UseAlbedoTex == 1){
       outColor *= texture(uMaterial.AlbedoTex, fs_in.TexCoords);
    }

    fragColor = outColor;
}
