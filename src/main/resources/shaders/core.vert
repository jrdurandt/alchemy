#version 450 core

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProjection;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoords;
layout(location = 2) in vec3 inNormal;

out VS_OUT {
    vec2 TexCoords;
    vec3 Normal;
} vs_out;

void main() {
    gl_Position = uProjection * uView * uModel * vec4(inPosition, 1.0);
    vs_out.TexCoords = inTexCoords;
    vs_out.Normal = inNormal;
}
