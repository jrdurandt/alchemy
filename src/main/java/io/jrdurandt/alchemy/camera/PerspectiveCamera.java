package io.jrdurandt.alchemy.camera;

public class PerspectiveCamera extends Camera {
	private float fov;
	private float aspectRatio;
	private float near;
	private float far;

	public float getFov() {
		return fov;
	}

	public void setFov(float fov) {
		this.fov = fov;
	}

	public float getAspectRatio() {
		return aspectRatio;
	}

	public void setAspectRatio(float aspectRatio) {
		this.aspectRatio = aspectRatio;
	}

	public float getNear() {
		return near;
	}

	public void setNear(float near) {
		this.near = near;
	}

	public float getFar() {
		return far;
	}

	public void setFar(float far) {
		this.far = far;
	}

	public PerspectiveCamera(float fov, float aspectRatio, float near, float far) {
		this.fov = fov;
		this.aspectRatio = aspectRatio;
		this.near = near;
		this.far = far;
		updateProjectionMatrix();
	}

	@Override
	public void updateProjectionMatrix() {
		this.projectionMatrix.identity().perspective(
				fov,
				aspectRatio,
				near,
				far
		);
	}
}
