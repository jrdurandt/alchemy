package io.jrdurandt.alchemy.camera;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public abstract class Camera {
	private final Vector3f position = new Vector3f();
	private final Vector3f target = new Vector3f();
	private final Vector3f up = new Vector3f(0.0f, -1.0f, 0.0f);
	private final Matrix4f viewMatrix = new Matrix4f().identity();
	protected final Matrix4f projectionMatrix = new Matrix4f().identity();

	public Vector3f position() {
		return position;
	}

	public Vector3f target() {
		return target;
	}

	public Vector3f up() {
		return up;
	}

	public Matrix4f getViewMatrix() {
		return viewMatrix;
	}

	public Matrix4f getProjectionMatrix() {
		return projectionMatrix;
	}

	public Camera() {
	}

	public void update() {
		this.viewMatrix.identity().lookAt(
				position,
				target,
				up
		);
	}

	public abstract void updateProjectionMatrix();
}
