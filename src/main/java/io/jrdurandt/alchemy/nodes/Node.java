package io.jrdurandt.alchemy.nodes;

import io.jrdurandt.alchemy.camera.Camera;
import io.jrdurandt.alchemy.core.Transform;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class Node {
	private final UUID uuid = UUID.randomUUID();

	private final String name;

	private Node parent;
	public final Set<Node> children = new HashSet<>();

	public final Set<Mesh> meshes = new HashSet<>();

	private Transform transform = new Transform();

	public Transform getTransform() {
		return transform;
	}

	public Optional<Node> getParent() {
		if (parent != null) {
			return Optional.of(parent);
		}
		return Optional.empty();
	}

	public Node(String name) {
		this.name = name;
	}

	public void add(Node node) {
		if (node == this) {
			throw new IllegalStateException("Node cannot be added to itself");
		}

		node.parent = this;
		this.children.add(node);
	}

	public void remove(Node node) {
		node.parent = null;
		children.remove(node);
	}

	public Optional<Node> findById(String id) {
		return children.stream()
				.filter(node -> node.uuid == UUID.fromString(id))
				.findFirst();
	}

	public Optional<Node> findByName(String name) {
		return children.stream()
				.filter(node -> node.name.equalsIgnoreCase(name))
				.findFirst();
	}

	protected void process(Camera camera) {
		for (var child : children) {
			child.draw(camera);
			child.process(camera);
		}
	}

	protected void destroy() {
		for (var child : children) {
			child.destroy();
			child.dispose();
		}
	}

	protected void draw(Camera camera){
		if(parent != null){
			transform.setParentTransform(parent.getTransform());
		}

		for(var mesh: meshes){
			mesh.draw(this, camera);
		}
	}

	protected void dispose() {
		meshes.forEach(Mesh::dispose);
	}
}
