package io.jrdurandt.alchemy.nodes;

import io.jrdurandt.alchemy.camera.Camera;
import io.jrdurandt.alchemy.core.Transform;
import io.jrdurandt.alchemy.geom.Geometry;
import io.jrdurandt.alchemy.materials.Material;

public class Mesh {
	private Transform transform = new Transform();
	private final Geometry geometry;
	private final Material material;

	public Mesh(String name, Geometry geometry, Material material) {
		this.geometry = geometry;
		this.material = material;
	}

	public void draw(Node node, Camera camera) {
		transform.setParentTransform(node.getTransform());
		material.apply(camera, transform, mat -> geometry.draw());
	}

	public void dispose() {
		geometry.delete();
	}
}
