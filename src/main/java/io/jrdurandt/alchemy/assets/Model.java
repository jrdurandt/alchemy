package io.jrdurandt.alchemy.assets;

import io.jrdurandt.alchemy.core.Color;
import io.jrdurandt.alchemy.core.Shader;
import io.jrdurandt.alchemy.geom.Geometry;
import io.jrdurandt.alchemy.geom.Vertex;
import io.jrdurandt.alchemy.materials.Material;
import io.jrdurandt.alchemy.nodes.Mesh;
import io.jrdurandt.alchemy.nodes.Node;
import io.jrdurandt.alchemy.textures.Texture2D;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.assimp.*;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class Model {
	private final Map<Integer, Texture2D> textures = new HashMap<>();
	private final Map<Integer, Material> materials = new HashMap<>();
	private final Map<Integer, Mesh> meshes = new HashMap<>();
	private Node rootNode;

	public Node getRootNode() {
		return rootNode;
	}

	public Model(String path) throws Exception {
		var fileIo = AIFileIO.create()
				.OpenProc((pFileIO, fileName, openMode) -> {
					ByteBuffer data;
					try {
						data = AssetLoader.loadResource(path);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}

					return AIFile.create()
							.ReadProc((pFile, pBuffer, size, count) -> {
								var max = Math.min(data.remaining(), size * count);
								MemoryUtil.memCopy(MemoryUtil.memAddress(data) + data.position(), pBuffer, max);
								return max;
							})
							.SeekProc((pFile, offset, origin) -> {
								if (origin == Assimp.aiOrigin_CUR) {
									data.position(data.position() + (int) offset);
								} else if (origin == Assimp.aiOrigin_SET) {
									data.position((int) offset);
								} else if (origin == Assimp.aiOrigin_END) {
									data.position(data.limit() + (int) offset);
								}
								return 0;
							})
							.FileSizeProc(pFile -> data.limit())
							.address();
				})
				.CloseProc((pFileIO, pFile) -> {
					var aiFile = AIFile.create(pFile);
					aiFile.ReadProc().free();
					aiFile.SeekProc().free();
					aiFile.FileSizeProc().free();
				});

		var scene = Assimp.aiImportFileEx(path,
				Assimp.aiProcess_JoinIdenticalVertices | Assimp.aiProcess_Triangulate,
				fileIo);
		fileIo.OpenProc().free();
		fileIo.CloseProc().free();
		if (scene == null) {
			throw new IllegalStateException(Assimp.aiGetErrorString());
		}

		var textureCount = scene.mNumTextures();
		var texturesBuffer = scene.mTextures();
		if (texturesBuffer != null) {
			for (var i = 0; i < textureCount; i++) {
				textures.put(i, createTexture(AITexture.create(texturesBuffer.get(i))));
			}
		}

		var materialCount = scene.mNumMaterials();
		var materialsBuffer = scene.mMaterials();
		if (materialsBuffer != null) {
			for (var i = 0; i < materialCount; i++) {
				materials.put(i, createMaterial(AIMaterial.create(materialsBuffer.get(i))));
			}
		}

		var meshCount = scene.mNumMeshes();
		var meshesBuffer = scene.mMeshes();
		if (meshesBuffer != null) {
			for (var i = 0; i < meshCount; i++) {
				meshes.put(i, createMesh(AIMesh.create(meshesBuffer.get(i))));
			}
		}

		var aiRootNode = scene.mRootNode();
		if (aiRootNode != null) {
			rootNode = populateMeshNode(aiRootNode);
		}
	}

	private Texture2D createTexture(AITexture aiTexture) throws IOException {
		var address = aiTexture.pcData().address0();
		var buffer = MemoryUtil.memByteBuffer(address, aiTexture.mWidth());
		return new Texture2D(new Image(buffer));
	}

	private Material createMaterial(AIMaterial aiMaterial) {
		var diffuseTextureIndex = 0;
		var texCount = Assimp.aiGetMaterialTextureCount(aiMaterial, Assimp.aiTextureType_DIFFUSE);
		if (texCount > 0) {
			try (var stack = MemoryStack.stackPush()) {
				var pPath = AIString.calloc(stack);
				var pMapping = stack.mallocInt(1);
				var pUvIndex = stack.mallocInt(1);
				var pBlend = stack.mallocFloat(1);
				var pOp = stack.mallocInt(1);
				var pMapMode = stack.mallocInt(1);
				var pFlags = stack.mallocInt(1);
				for (var i = 0; i < texCount; i++) {
					Assimp.aiGetMaterialTexture(aiMaterial, Assimp.aiTextureType_DIFFUSE, i,
							pPath,
							pMapping,
							pUvIndex,
							pBlend,
							pOp,
							pMapMode,
							pFlags);
//					LOGGER.debug("Path:{}, Mapping:{}, UVIndex:{}, Blend:{}, Op:{}, MM:{}",
//							pPath.dataString(),
//							pMapping.get(0),
//							pUvIndex.get(0),
//							pBlend.get(0),
//							pOp.get(0),
//							pMapMode.get(0));
					diffuseTextureIndex = Integer.parseInt(pPath.dataString().substring(1));
					;
				}
			}
		}

		return new Material(Shader.DEFAULT_SHADER, Color.White, textures.get(diffuseTextureIndex));
	}

	private Mesh createMesh(AIMesh aiMesh) {
		var name = aiMesh.mName().dataString();
		var numVertices = aiMesh.mNumVertices();
		var vertices = new Vertex[numVertices];

		var pVertices = aiMesh.mVertices();
		var pTexCoords = aiMesh.mTextureCoords(0);
		var pNormals = aiMesh.mNormals();
		for (var i = 0; i < numVertices; i++) {
			var aiVertex = pVertices.get(i);
			var position = new Vector3f(
					aiVertex.x(),
					aiVertex.y(),
					aiVertex.z()
			);

			var texCoord = new Vector2f();
			if (pTexCoords != null) {
				var aiTexCoord = pTexCoords.get(i);
				texCoord = new Vector2f(
						aiTexCoord.x(),
						aiTexCoord.y()
				);
			}

			var normal = new Vector3f();
			if (pNormals != null) {
				var aiNormal = pNormals.get(i);
				normal = new Vector3f(
						aiNormal.x(),
						aiNormal.y(),
						aiNormal.z()
				);
			}

			vertices[i] = new Vertex(
					position,
					texCoord,
					normal
			);
		}

		var faceCount = aiMesh.mNumFaces();
		var indices = new int[faceCount * 3];

		var faces = aiMesh.mFaces();
		for (int f = 0, i = 0; f < faceCount; f++, i += 3) {
			var face = faces.get(f);
			if (face.mNumIndices() != 3) {
				throw new IllegalStateException("AIFace.mNumIndices() != 3");
			}
			var aiIndices = face.mIndices();
			indices[i] = aiIndices.get(0);
			indices[i + 1] = aiIndices.get(1);
			indices[i + 2] = aiIndices.get(2);
		}

		var geometry = new Geometry(vertices, indices);
		var material = this.materials.get(aiMesh.mMaterialIndex());
		return new Mesh(name, geometry, material);
	}

	private Node populateMeshNode(AINode aiNode) {
		if (aiNode != null) {
			Node node = new Node(aiNode.mName().dataString());
			var meshIndices = aiNode.mMeshes();
			if (meshIndices != null) {
				node.meshes.add(meshes.get(meshIndices.get(0)));
			}

			var childrenCount = aiNode.mNumChildren();
			var childrenBuffer = aiNode.mChildren();
			if (childrenBuffer != null) {
				for (var i = 0; i < childrenCount; i++) {
					var child = AINode.create(childrenBuffer.get(i));
					var childNode = populateMeshNode(child);
					if (childNode != null) {
						node.add(childNode);
					}
				}
			}
			return node;
		}
		return null;
	}
}
