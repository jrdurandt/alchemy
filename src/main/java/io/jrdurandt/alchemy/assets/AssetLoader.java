package io.jrdurandt.alchemy.assets;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class AssetLoader {
	public static ByteBuffer loadResource(String path) throws IOException {
		var url = AssetLoader.class.getClassLoader().getResource(path);
		if (url == null) {
			throw new FileNotFoundException("Failed to find resource file at " + path);
		}
		return load(url.getFile());
	}

	public static ByteBuffer load(String path) throws IOException {
		try (var fileInputStream = new FileInputStream(path)) {
			try (var channel = fileInputStream.getChannel()) {
				return channel.map(FileChannel.MapMode.READ_ONLY, channel.position(), channel.size());
			}
		}
	}
}
