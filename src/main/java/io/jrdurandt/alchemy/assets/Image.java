package io.jrdurandt.alchemy.assets;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.stb.STBImage;
import org.lwjgl.system.MemoryStack;

import java.io.IOException;
import java.nio.ByteBuffer;

public class Image {
	private final int width;
	private final int height;
	private final int channels;
	private final ByteBuffer data;

	private final boolean is16bits;
	private final boolean isHdr;

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getChannels() {
		return channels;
	}

	public ByteBuffer getData() {
		return data;
	}

	public boolean is16bits() {
		return is16bits;
	}

	public boolean isHdr() {
		return isHdr;
	}

	public Image(ByteBuffer raw) throws IOException {
		try (var stack = MemoryStack.stackPush()) {
			var pWidth = stack.mallocInt(1);
			var pHeight = stack.mallocInt(1);
			var pChannels = stack.mallocInt(1);

			this.data = STBImage.stbi_load_from_memory(raw, pWidth, pHeight, pChannels, 0);
			if (data == null) {
				throw new IOException("Failed to load image: " + STBImage.stbi_failure_reason());
			}

			this.width = pWidth.get(0);
			this.height = pHeight.get(0);
			this.channels = pChannels.get(0);

			this.is16bits = STBImage.stbi_is_16_bit_from_memory(raw);
			this.isHdr = STBImage.stbi_is_hdr_from_memory(raw);
		}
	}

	public Image(String path) throws IOException {
		this(AssetLoader.loadResource(path));
	}

	public int getPixelFormat() {
		return switch (channels) {
			case 4 -> GL11.GL_RGBA;
			case 3 -> GL11.GL_RGB;
			case 1 -> GL11.GL_RED;
			default -> throw new IllegalStateException("Invalid or unsupported image format for channels: " + channels);
		};
	}

	public int getInternalFormat() {
		if (is16bits) {
			return switch (channels) {
				case 4 -> GL30.GL_RGBA16F;
				case 3 -> GL30.GL_RGB16F;
				case 1 -> GL11.GL_RED;
				default ->
						throw new IllegalStateException("Invalid or unsupported image format for channels: " + channels);
			};
		} else {
			return switch (channels) {
				case 4 -> GL30.GL_RGBA32F;
				case 3 -> GL30.GL_RGB32F;
				case 1 -> GL11.GL_RED;
				default ->
						throw new IllegalStateException("Invalid or unsupported image format for channels: " + channels);
			};
		}
	}
}
