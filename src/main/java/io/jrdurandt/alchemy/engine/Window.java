package io.jrdurandt.alchemy.engine;

import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;

public final class Window {
	private int width, height;

	private final long handle;
	private ResizeCallback resizeCallback;

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Window(Settings settings) {
		this.width = settings.getWidth();
		this.height = settings.getHeight();

		if (!glfwInit()) {
			throw new IllegalStateException("Failed to init GLFW");
		}

		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

		this.handle = glfwCreateWindow(
				width,
				height,
				settings.getTitle(),
				0, 0
		);
		if (handle == GLFW_FALSE) {
			throw new IllegalStateException("Failed to create GLFW window");
		}
		glfwMakeContextCurrent(handle);
		if (settings.isVSyncEnabled()) {
			glfwSwapInterval(1);
		}
		GL.createCapabilities();

		glfwSetWindowSizeCallback(handle, (window, width, height) -> {
			this.width = width;
			this.height = height;
			if(resizeCallback != null) {
				resizeCallback.invoke(width, height);
			}
		});
	}

	public void onResize(ResizeCallback callback){
		this.resizeCallback = callback;
	}

	public void show() {
		glfwShowWindow(handle);
	}

	public boolean shouldClose() {
		return glfwWindowShouldClose(handle);
	}

	public void swapBuffers() {
		glfwSwapBuffers(handle);
	}

	public void close() {
		glfwSetWindowShouldClose(handle, true);
	}

	public void destroy() {
		glfwDestroyWindow(handle);
		glfwTerminate();
	}

	public interface ResizeCallback{
		void invoke(int width, int height);
	}
}
