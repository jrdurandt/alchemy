package io.jrdurandt.alchemy.engine;

import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.util.Map;

public class Settings {
	public static String SETTINGS_FILE_PATH = "settings.yaml";
	private static Settings instance;

	public static Settings getInstance() {
		if (instance == null) {
			instance = new Settings();
		}
		return instance;
	}

	private String title = "";
	private boolean debugEnabled;
	private int width = 800;
	private int height = 600;
	private boolean vSyncEnabled;

	private Settings() {
		try (var is = Settings.class.getClassLoader().getResourceAsStream(SETTINGS_FILE_PATH)) {
			var yaml = new Yaml();

			Map<String, Object> data = yaml.load(is);
			this.title = (String) data.getOrDefault("title", title);
			this.debugEnabled = (boolean) data.getOrDefault("debug_enabled", false);
			this.width = (int) data.getOrDefault("width", width);
			this.height = (int) data.getOrDefault("height", height);
			this.vSyncEnabled = (boolean) data.getOrDefault("v_sync_enabled", false);

		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isDebugEnabled() {
		return debugEnabled;
	}

	public void setDebugEnabled(boolean debugEnabled) {
		this.debugEnabled = debugEnabled;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isVSyncEnabled() {
		return vSyncEnabled;
	}

	public void setVSyncEnabled(boolean vSyncEnabled) {
		this.vSyncEnabled = vSyncEnabled;
	}
}
