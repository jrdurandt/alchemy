package io.jrdurandt.alchemy.materials;

import io.jrdurandt.alchemy.camera.Camera;
import io.jrdurandt.alchemy.core.Color;
import io.jrdurandt.alchemy.core.Shader;
import io.jrdurandt.alchemy.core.Transform;
import io.jrdurandt.alchemy.textures.Texture2D;

import java.util.Objects;
import java.util.function.Consumer;

public class Material {

	private final Shader shader;
	private Color albedoColor = Color.White;
	private Texture2D albedoTex;

	public Shader getShader() {
		return shader;
	}

	public Color getAlbedoColor() {
		return albedoColor;
	}

	public void setAlbedoColor(Color albedoColor) {
		this.albedoColor = albedoColor;
	}

	public Texture2D getAlbedoTex() {
		return albedoTex;
	}

	public void setAlbedoTex(Texture2D albedoTex) {
		this.albedoTex = albedoTex;
	}

	public Material(Shader shader, Color albedoColor, Texture2D albedoTex) {
		this.shader = shader;
		this.albedoColor = albedoColor;
		this.albedoTex = albedoTex;
	}

	public void apply(Camera camera, Transform transform, Consumer<Material> block) {
		shader.use(fx -> {
			fx.setUniform("uView", camera.getViewMatrix());
			fx.setUniform("uProjection", camera.getProjectionMatrix());
			fx.setUniform("uModel", transform.getModelMatrix());

			fx.setUniform("uMaterial.AlbedoColor",
					Objects.requireNonNullElse(albedoColor, Color.White));

			if (albedoTex != null) {
				fx.setUniform("uMaterial.UseAlbedoTex", true);
				fx.setUniform("uMaterial.AlbedoTex", 0);
				albedoTex.bind(0);
			} else {
				fx.setUniform("uMaterial.UseAlbedoTex", false);
			}

			block.accept(this);
		});

	}
}
