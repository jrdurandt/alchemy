package io.jrdurandt.alchemy.textures;

import io.jrdurandt.alchemy.assets.Image;
import org.lwjgl.opengl.*;

import java.io.IOException;
import java.nio.ByteBuffer;

public class Texture2D implements Texture {
	private final int id;

	public Texture2D(int internalFormat, int width, int height, int format, ByteBuffer data) {
		this.id = GL45.glCreateTextures(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, id);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0,
				internalFormat,
				width,
				height,
				0,
				format,
				GL11.GL_UNSIGNED_BYTE,
				data);

		GL45.glTextureParameteri(id, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		GL45.glTextureParameteri(id, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

		GL45.glTextureParameteri(id, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		if (Texture.isPowerOfTwo(width) && Texture.isPowerOfTwo(height)) {
			GL45.glGenerateTextureMipmap(id);
			GL45.glTextureParameteri(id, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_NEAREST);
		} else {
			GL45.glTextureParameteri(id, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		}
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}

	public Texture2D(Image image) {
		this(image.getInternalFormat(), image.getWidth(), image.getHeight(), image.getPixelFormat(), image.getData());
	}

	public Texture2D(String path) throws IOException {
		this(new Image(path));
	}

	@Override
	public void bind(int unit) {
		GL45.glBindTextureUnit(unit, id);
	}

	@Override
	public void delete() {
		GL11.glDeleteTextures(id);
	}

}
