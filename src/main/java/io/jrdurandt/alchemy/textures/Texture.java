package io.jrdurandt.alchemy.textures;

public interface Texture {
	void bind(int unit);

	void delete();

	static boolean isPowerOfTwo(int n) {
		return (n & n - 1) == 0;
	}
}
