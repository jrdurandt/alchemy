package io.jrdurandt.alchemy.core;

import io.jrdurandt.alchemy.assets.AssetLoader;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

public final class Shader {
	public static final Shader DEFAULT_SHADER;

	static {
		try {
			DEFAULT_SHADER = new Shader(
					AssetLoader.loadResource("shaders/core.vert"),
					AssetLoader.loadResource("shaders/core.frag"));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private final int program;

	private final Map<String, Integer> attribLocationMap;
	private final Map<String, Integer> uniformLocationMap;

	public Shader(ByteBuffer vsSource, ByteBuffer fsSource) throws Exception {
		this.program = GL20.glCreateProgram();
		var vs = initShader(vsSource, GL20.GL_VERTEX_SHADER);
		var fs = initShader(fsSource, GL20.GL_FRAGMENT_SHADER);
		GL20.glLinkProgram(program);
		disposeShader(vs);
		disposeShader(fs);

		var linkStatus = GL20.glGetProgrami(program, GL20.GL_LINK_STATUS);
		if (linkStatus != GL11.GL_TRUE) {
			var infoLog = GL20.glGetProgramInfoLog(program);
			delete();
			throw new Exception("Failed to links shader program: " + infoLog);
		}

		this.attribLocationMap = cacheAttribLocationMap();
		this.uniformLocationMap = cacheUniformLocationMap();
	}

	public Shader(String vsPath, String fsPath) throws Exception {
		this(AssetLoader.loadResource(vsPath), AssetLoader.loadResource(fsPath));
	}

	private int initShader(ByteBuffer source, int type) throws Exception {
		var sourceStr = MemoryUtil.memUTF8(source);

		var shader = GL20.glCreateShader(type);
		GL20.glShaderSource(shader, sourceStr);
		GL20.glCompileShader(shader);

		var compileStatus = GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS);
		if (compileStatus != GL11.GL_TRUE) {
			var infoLog = GL20.glGetShaderInfoLog(shader);
			GL20.glDeleteShader(shader);
			throw new Exception("Failed to compile shader [" + type + "]: " + infoLog);
		}
		GL20.glAttachShader(program, shader);
		return shader;
	}

	private void disposeShader(int shader) {
		GL20.glDetachShader(program, shader);
		GL20.glDeleteShader(shader);
	}

	private Map<String, Integer> cacheAttribLocationMap() {
		var attribLocationMap = new HashMap<String, Integer>();

		var count = GL20.glGetProgrami(program, GL20.GL_ACTIVE_ATTRIBUTES);
		try (var stack = MemoryStack.stackPush()) {
			var pSize = stack.mallocInt(1);
			var pType = stack.mallocInt(1);

			for (var i = 0; i < count; i++) {
				var name = GL20.glGetActiveAttrib(program, i, pSize, pType);
				var location = GL20.glGetAttribLocation(program, name);
				attribLocationMap.put(name, location);
			}
		}
		return attribLocationMap;
	}

	private Map<String, Integer> cacheUniformLocationMap() {
		var uniformLocationMap = new HashMap<String, Integer>();

		var count = GL20.glGetProgrami(program, GL20.GL_ACTIVE_UNIFORMS);
		try (var stack = MemoryStack.stackPush()) {
			var pSize = stack.mallocInt(1);
			var pType = stack.mallocInt(1);

			for (var i = 0; i < count; i++) {
				var name = GL20.glGetActiveUniform(program, i, pSize, pType);
				var location = GL20.glGetUniformLocation(program, name);
				uniformLocationMap.put(name, location);
			}
		}
		return uniformLocationMap;
	}

	public void delete() {
		GL20.glDeleteProgram(program);
	}

	public Optional<Integer> getAttribLocation(String name) {
		var location = attribLocationMap.getOrDefault(name,
				GL20.glGetAttribLocation(program, name));
		if (location != -1) {
			return Optional.of(location);
		}
		return Optional.empty();
	}

	public Optional<Integer> getUniformLocation(String name) {
		var location = uniformLocationMap.getOrDefault(name,
				GL20.glGetUniformLocation(program, name));
		if (location != -1) {
			return Optional.of(location);
		}
		return Optional.empty();
	}


	public void use(Consumer<Shader> block) {
		GL20.glUseProgram(program);
		block.accept(this);
		GL20.glUseProgram(0);
	}

	public void setUniformInt(String name, int val) {
		getUniformLocation(name)
				.ifPresent(location -> GL20.glUniform1i(location, val));
	}

	public void setUniformFloat(String name, float val) {
		getUniformLocation(name)
				.ifPresent(location ->
						GL20.glUniform1f(location, val));
	}

	public void setUniformVector2f(String name, Vector2f val) {
		getUniformLocation(name)
				.ifPresent(location ->
						GL20.glUniform2f(location, val.x, val.y));
	}

	public void setUniformVector3f(String name, Vector3f val) {
		getUniformLocation(name)
				.ifPresent(location ->
						GL20.glUniform3f(location, val.x, val.y, val.z));
	}

	public void setUniformVector4f(String name, Vector4f val) {
		getUniformLocation(name)
				.ifPresent(location ->
						GL20.glUniform4f(location, val.x, val.y, val.z, val.w));
	}

	public void setUniformMatrix4f(String name, Matrix4f val) {
		getUniformLocation(name)
				.ifPresent(location ->
						GL20.glUniformMatrix4fv(location, false, val.get(new float[16])));
	}

	public void setUniform(String name, Object val) {
		if (val instanceof Integer v) {
			setUniformInt(name, v);
		} else if (val instanceof Float v) {
			setUniformFloat(name, v);
		} else if (val instanceof Vector2f v) {
			setUniformVector2f(name, v);
		} else if (val instanceof Vector3f v) {
			setUniformVector3f(name, v);
		} else if (val instanceof Vector4f v) {
			setUniformVector4f(name, v);
		} else if (val instanceof Matrix4f v) {
			setUniformMatrix4f(name, v);
		} else if (val instanceof Boolean v) {
			setUniformInt(name, v ? 1 : 0);
		} else {
			throw new IllegalStateException("Invalid uniform type: " + val.getClass().getSimpleName());
		}
	}

}
