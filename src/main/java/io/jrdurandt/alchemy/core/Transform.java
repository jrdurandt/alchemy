package io.jrdurandt.alchemy.core;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

public class Transform {
	private final Vector3f position = new Vector3f();
	private final Quaternionf rotation = new Quaternionf();
	private final Vector3f scale = new Vector3f(1.0f);

	private final Matrix4f modelMatrix = new Matrix4f().identity();

	private Transform parentTransform;

	public Vector3f position() {
		return position;
	}

	public Quaternionf rotation() {
		return rotation;
	}

	public Vector3f scale() {
		return scale;
	}

	public void setParentTransform(Transform parentTransform) {
		this.parentTransform = parentTransform;
	}

	public Matrix4f getModelMatrix() {
		this.modelMatrix.identity()
				.translationRotateScale(
						position,
						rotation,
						scale
				);

		if(parentTransform != null){
			this.modelMatrix.mulLocal(parentTransform.getModelMatrix());
		}
		return modelMatrix;
	}
}
