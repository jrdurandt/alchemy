package io.jrdurandt.alchemy.core;

import io.jrdurandt.alchemy.camera.Camera;
import io.jrdurandt.alchemy.nodes.Node;

import java.util.Optional;
import java.util.Set;

public class Scene extends Node {
	public Scene(String name) {
		super(name);
	}

	private Optional<Node> iterateChildren(String name, Set<Node> children) {
		for (var child : children) {
			var node = child.findByName(name);
			if (node.isPresent()) {
				return node;
			}
			return iterateChildren(name, child.children);
		}
		return Optional.empty();
	}

	public Optional<Node> findNodeInSceneTree(String name) {
		return iterateChildren(name, children);
	}

	@Override
	public void draw(Camera camera) {
		process(camera);
	}

	@Override
	public void dispose() {
		destroy();
	}
}
