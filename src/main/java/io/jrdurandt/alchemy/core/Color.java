package io.jrdurandt.alchemy.core;

import org.joml.Vector4f;

public class Color extends Vector4f {

	public float r(){
		return this.x;
	}

	public void r(float r){
		this.x = r;
	}

	public float g(){
		return this.y;
	}

	public void g(float g){
		this.y = g;
	}

	public float b(){
		return this.z;
	}

	public void b(float b){
		this.z = b;
	}

	public float a(){
		return this.w;
	}

	public void a(float a){
		this.w = a;
	}

	public Color() {
	}

	public Color(float r, float g, float b, float a) {
		super(r, g, b, a);
	}

	public Color(float d) {
		super(d);
	}

	public Color(int rgba){
		this(
				((rgba & 0xFF000000) >>> 24) / 255f,
				((rgba & 0x00FF0000) >>> 16) / 255f,
				((rgba & 0x0000FF00) >>> 8) / 255f,
				((rgba & 0x000000FF)) / 255f
		);
	}

	public static final Color IndianRed = new Color(0xCD5C5CFF);
	public static final Color LightCoral = new Color(0xF08080FF);
	public static final Color Salmon = new Color(0xFA8072FF);
	public static final Color DarkSalmon = new Color(0xE9967AFF);
	public static final Color LightSalmon = new Color(0xFFA07AFF);
	public static final Color Crimson = new Color(0xDC143CFF);
	public static final Color Red = new Color(0xFF0000FF);
	public static final Color FireBrick = new Color(0xB22222FF);
	public static final Color DarkRed = new Color(0x8B0000FF);
	public static final Color Pink = new Color(0xFFC0CBFF);
	public static final Color LightPink = new Color(0xFFB6C1FF);
	public static final Color HotPink = new Color(0xFF69B4FF);
	public static final Color DeepPink = new Color(0xFF1493FF);
	public static final Color MediumVioletRed = new Color(0xC71585FF);
	public static final Color PaleVioletRed = new Color(0xDB7093FF);
	public static final Color Coral = new Color(0xFF7F50FF);
	public static final Color Tomato = new Color(0xFF6347FF);
	public static final Color OrangeRed = new Color(0xFF4500FF);
	public static final Color DarkOrange = new Color(0xFF8C00FF);
	public static final Color Orange = new Color(0xFFA500FF);
	public static final Color Gold = new Color(0xFFD700FF);
	public static final Color Yellow = new Color(0xFFFF00FF);
	public static final Color LightYellow = new Color(0xFFFFE0FF);
	public static final Color LemonChiffon = new Color(0xFFFACDFF);
	public static final Color LightGoldenrodYellow = new Color(0xFAFAD2FF);
	public static final Color PapayaWhip = new Color(0xFFEFD5FF);
	public static final Color Moccasin = new Color(0xFFE4B5FF);
	public static final Color PeachPuff = new Color(0xFFDAB9FF);
	public static final Color PaleGoldenrod = new Color(0xEEE8AAFF);
	public static final Color Khaki = new Color(0xF0E68CFF);
	public static final Color DarkKhaki = new Color(0xBDB76BFF);
	public static final Color Lavender = new Color(0xE6E6FAFF);
	public static final Color Thistle = new Color(0xD8BFD8FF);
	public static final Color Plum = new Color(0xDDA0DDFF);
	public static final Color Violet = new Color(0xEE82EEFF);
	public static final Color Orchid = new Color(0xDA70D6FF);
	public static final Color Fuchsia = new Color(0xFF00FFFF);
	public static final Color Magenta = new Color(0xFF00FFFF);
	public static final Color MediumOrchid = new Color(0xBA55D3FF);
	public static final Color MediumPurple = new Color(0x9370DBFF);
	public static final Color Amethyst = new Color(0x9966CCFF);
	public static final Color BlueViolet = new Color(0x8A2BE2FF);
	public static final Color DarkViolet = new Color(0x9400D3FF);
	public static final Color DarkOrchid = new Color(0x9932CCFF);
	public static final Color DarkMagenta = new Color(0x8B008BFF);
	public static final Color Purple = new Color(0x800080FF);
	public static final Color Indigo = new Color(0x4B0082FF);
	public static final Color SlateBlue = new Color(0x6A5ACDFF);
	public static final Color DarkSlateBlue = new Color(0x483D8BFF);
	public static final Color MediumSlateBlue = new Color(0x7B68EEFF);
	public static final Color GreenYellow = new Color(0xADFF2FFF);
	public static final Color Chartreuse = new Color(0x7FFF00FF);
	public static final Color LawnGreen = new Color(0x7CFC00FF);
	public static final Color Lime = new Color(0x00FF00FF);
	public static final Color LimeGreen = new Color(0x32CD32FF);
	public static final Color PaleGreen = new Color(0x98FB98FF);
	public static final Color LightGreen = new Color(0x90EE90FF);
	public static final Color MediumSpringGreen = new Color(0x00FA9AFF);
	public static final Color SpringGreen = new Color(0x00FF7FFF);
	public static final Color MediumSeaGreen = new Color(0x3CB371FF);
	public static final Color SeaGreen = new Color(0x2E8B57FF);
	public static final Color ForestGreen = new Color(0x228B22FF);
	public static final Color Green = new Color(0x008000FF);
	public static final Color DarkGreen = new Color(0x006400FF);
	public static final Color YellowGreen = new Color(0x9ACD32FF);
	public static final Color OliveDrab = new Color(0x6B8E23FF);
	public static final Color Olive = new Color(0x808000FF);
	public static final Color DarkOliveGreen = new Color(0x556B2FFF);
	public static final Color MediumAquamarine = new Color(0x66CDAAFF);
	public static final Color DarkSeaGreen = new Color(0x8FBC8FFF);
	public static final Color LightSeaGreen = new Color(0x20B2AAFF);
	public static final Color DarkCyan = new Color(0x008B8BFF);
	public static final Color Teal = new Color(0x008080FF);
	public static final Color Aqua = new Color(0x00FFFFFF);
	public static final Color Cyan = new Color(0x00FFFFFF);
	public static final Color LightCyan = new Color(0xE0FFFFFF);
	public static final Color PaleTurquoise = new Color(0xAFEEEEFF);
	public static final Color Aquamarine = new Color(0x7FFFD4FF);
	public static final Color Turquoise = new Color(0x40E0D0FF);
	public static final Color MediumTurquoise = new Color(0x48D1CCFF);
	public static final Color DarkTurquoise = new Color(0x00CED1FF);
	public static final Color CadetBlue = new Color(0x5F9EA0FF);
	public static final Color SteelBlue = new Color(0x4682B4FF);
	public static final Color LightSteelBlue = new Color(0xB0C4DEFF);
	public static final Color PowderBlue = new Color(0xB0E0E6FF);
	public static final Color LightBlue = new Color(0xADD8E6FF);
	public static final Color SkyBlue = new Color(0x87CEEBFF);
	public static final Color LightSkyBlue = new Color(0x87CEFAFF);
	public static final Color DeepSkyBlue = new Color(0x00BFFFFF);
	public static final Color DodgerBlue = new Color(0x1E90FFFF);
	public static final Color CornflowerBlue = new Color(0x6495EDFF);
	public static final Color RoyalBlue = new Color(0x4169E1FF);
	public static final Color Blue = new Color(0x0000FFFF);
	public static final Color MediumBlue = new Color(0x0000CDFF);
	public static final Color DarkBlue = new Color(0x00008BFF);
	public static final Color Navy = new Color(0x000080FF);
	public static final Color MidnightBlue = new Color(0x191970FF);
	public static final Color Cornsilk = new Color(0xFFF8DCFF);
	public static final Color BlanchedAlmond = new Color(0xFFEBCDFF);
	public static final Color Bisque = new Color(0xFFE4C4FF);
	public static final Color NavajoWhite = new Color(0xFFDEADFF);
	public static final Color Wheat = new Color(0xF5DEB3FF);
	public static final Color BurlyWood = new Color(0xDEB887FF);
	public static final Color Tan = new Color(0xD2B48CFF);
	public static final Color RosyBrown = new Color(0xBC8F8FFF);
	public static final Color SandyBrown = new Color(0xF4A460FF);
	public static final Color Goldenrod = new Color(0xDAA520FF);
	public static final Color DarkGoldenrod = new Color(0xB8860BFF);
	public static final Color Peru = new Color(0xCD853FFF);
	public static final Color Chocolate = new Color(0xD2691EFF);
	public static final Color SaddleBrown = new Color(0x8B4513FF);
	public static final Color Sienna = new Color(0xA0522DFF);
	public static final Color Brown = new Color(0xA52A2AFF);
	public static final Color Maroon = new Color(0x800000FF);
	public static final Color White = new Color(0xFFFFFFFF);
	public static final Color Snow = new Color(0xFFFAFAFF);
	public static final Color Honeydew = new Color(0xF0FFF0FF);
	public static final Color MintCream = new Color(0xF5FFFAFF);
	public static final Color Azure = new Color(0xF0FFFFFF);
	public static final Color AliceBlue = new Color(0xF0F8FFFF);
	public static final Color GhostWhite = new Color(0xF8F8FFFF);
	public static final Color WhiteSmoke = new Color(0xF5F5F5FF);
	public static final Color Seashell = new Color(0xFFF5EEFF);
	public static final Color Beige = new Color(0xF5F5DCFF);
	public static final Color OldLace = new Color(0xFDF5E6FF);
	public static final Color FloralWhite = new Color(0xFFFAF0FF);
	public static final Color Ivory = new Color(0xFFFFF0FF);
	public static final Color AntiqueWhite = new Color(0xFAEBD7FF);
	public static final Color Linen = new Color(0xFAF0E6FF);
	public static final Color LavenderBlush = new Color(0xFFF0F5FF);
	public static final Color MistyRose = new Color(0xFFE4E1FF);
	public static final Color Gainsboro = new Color(0xDCDCDCFF);
	public static final Color LightGrey = new Color(0xD3D3D3FF);
	public static final Color Silver = new Color(0xC0C0C0FF);
	public static final Color DarkGray = new Color(0xA9A9A9FF);
	public static final Color Gray = new Color(0x808080FF);
	public static final Color DimGray = new Color(0x696969FF);
	public static final Color LightSlateGray = new Color(0x778899FF);
	public static final Color SlateGray = new Color(0x708090FF);
	public static final Color DarkSlateGray = new Color(0x2F4F4FFF);
	public static final Color Black = new Color(0x000000FF);
}
