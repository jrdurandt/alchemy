package io.jrdurandt.alchemy.core;

import io.jrdurandt.alchemy.camera.Camera;
import org.lwjgl.opengl.GL11;

public class Renderer {
	private int width, height;

	private Scene currentScene;

	public Renderer() throws Exception {
		setClearColor(Color.DarkSlateBlue);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

	public void setClearColor(Color color) {
		GL11.glClearColor(color.r(), color.g(), color.b(), color.a());
	}

	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
		GL11.glViewport(0, 0, width, height);
	}

	public void render(Scene scene, Camera camera) {
		this.currentScene = scene;

		camera.update();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

		currentScene.draw(camera);
	}

	public void dispose() {
		if (currentScene != null) {
			currentScene.dispose();
		}
	}
}
