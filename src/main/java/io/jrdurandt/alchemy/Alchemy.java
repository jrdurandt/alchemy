package io.jrdurandt.alchemy;

import ch.qos.logback.classic.Logger;
import io.jrdurandt.alchemy.assets.Model;
import io.jrdurandt.alchemy.camera.PerspectiveCamera;
import io.jrdurandt.alchemy.core.Color;
import io.jrdurandt.alchemy.core.Renderer;
import io.jrdurandt.alchemy.core.Scene;
import io.jrdurandt.alchemy.engine.Settings;
import io.jrdurandt.alchemy.engine.Window;
import org.lwjgl.glfw.GLFW;
import org.slf4j.LoggerFactory;

public class Alchemy {
	public static final Logger LOGGER = (Logger) LoggerFactory.getLogger(Alchemy.class);

	public Alchemy() {

	}

	public void run() {
		var settings = Settings.getInstance();
		var window = new Window(settings);

		try {
			var scene = new Scene("ROOT");
			var camera = new PerspectiveCamera(75.0f,
					(float) window.getWidth() / (float) window.getHeight(),
					0.1f, 100.0f);
			var renderer = new Renderer();
			renderer.setClearColor(Color.CornflowerBlue);
			renderer.setSize(window.getWidth(), window.getHeight());

			window.onResize((width, height) -> {
				renderer.setSize(width, height);
				camera.setAspectRatio((float) width / (float) height);
				camera.updateProjectionMatrix();
			});

			var suzanne = new Model("models/suzanne_in_a_box.gltf");

			scene.add(suzanne.getRootNode());

			camera.position().x = 8.0f;
			camera.position().y = 8.0f;
			camera.position().z = 15.0f;

			window.show();
			while (!window.shouldClose()) {
				GLFW.glfwPollEvents();

				renderer.render(scene, camera);
				window.swapBuffers();
			}
			renderer.dispose();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			window.destroy();
		}
	}

	public static void main(String[] args) {
		var alchemy = new Alchemy();
		alchemy.run();
	}
}
