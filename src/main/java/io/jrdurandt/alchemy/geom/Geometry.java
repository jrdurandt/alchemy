package io.jrdurandt.alchemy.geom;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL45;

public class Geometry {

	private final int vao, vbo, ebo;
	private final int count;

	public Geometry(Vertex[] vertices, int[] indices) {
		this.count = indices.length;

		this.vao = GL45.glCreateVertexArrays();
		GL30.glBindVertexArray(vao);

		this.vbo = GL45.glCreateBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, Vertex.flatten(vertices), GL15.GL_STATIC_DRAW);

		this.ebo = GL45.glCreateBuffers();
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, ebo);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indices, GL15.GL_STATIC_DRAW);

		Vertex.enableVertexAttribs();

		GL30.glBindVertexArray(0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	public Geometry(GeometryData data) {
		this(data.getVertices(), data.getIndices());
	}

	public void delete() {
		GL30.glDeleteVertexArrays(vao);
		GL15.glDeleteBuffers(vbo);
		GL15.glDeleteBuffers(ebo);
	}

	public void draw() {
		GL30.glBindVertexArray(vao);
		GL11.glDrawElements(GL11.GL_TRIANGLES, count, GL11.GL_UNSIGNED_INT, 0);
		GL30.glBindVertexArray(0);
	}
}
