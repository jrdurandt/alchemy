package io.jrdurandt.alchemy.geom;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.util.par.ParShapes;

public final class GeometryData {
	private final Vertex[] vertices;
	private final int[] indices;

	public Vertex[] getVertices() {
		return vertices;
	}

	public int[] getIndices() {
		return indices;
	}

	public GeometryData(Vertex[] vertices, int[] indices) {
		this.vertices = vertices;
		this.indices = indices;
	}

	public static GeometryData quad(float width, float height) {
		return new GeometryData(
				Vertex.build(new float[]{
						-width / 2.0f, height / 2.0f, 0.0f,
						0.0f, 0.0f,
						0.0f, 0.0f, 1.0f,

						width / 2.0f, height / 2.0f, 0.0f,
						1.0f, 0.0f,
						0.0f, 0.0f, 1.0f,

						width / 2.0f, -height / 2.0f, 0.0f,
						1.0f, 1.0f,
						0.0f, 0.0f, 1.0f,

						-width / 2.0f, -height / 2.0f, 0.0f,
						0.0f, 1.0f,
						0.0f, 0.0f, 1.0f
				}),
				new int[]{
						0, 1, 2,
						2, 3, 0
				}
		);
	}

	public static GeometryData cube(float width, float height, float depth) {
		return new GeometryData(
				Vertex.build(new float[]{
						//Front
						-width / 2.0f, height / 2.0f, depth / 2.0f,
						0.0f, 0.0f,
						0.0f, 0.0f, 1.0f,

						width / 2.0f, height / 2.0f, depth / 2.0f,
						1.0f, 0.0f,
						0.0f, 0.0f, 1.0f,

						width / 2.0f, -height / 2.0f, depth / 2.0f,
						1.0f, 1.0f,
						0.0f, 0.0f, 1.0f,

						-width / 2.0f, -height / 2.0f, depth / 2.0f,
						0.0f, 1.0f,
						0.0f, 0.0f, 1.0f,

						//Right
						width / 2.0f, height / 2.0f, depth / 2.0f,
						0.0f, 0.0f,
						1.0f, 0.0f, 0.0f,

						width / 2.0f, height / 2.0f, -depth / 2.0f,
						1.0f, 0.0f,
						1.0f, 0.0f, 0.0f,

						width / 2.0f, -height / 2.0f, -depth / 2.0f,
						1.0f, 1.0f,
						1.0f, 0.0f, 0.0f,

						width / 2.0f, -height / 2.0f, depth / 2.0f,
						0.0f, 1.0f,
						1.0f, 0.0f, 0.0f,

						//Back
						width / 2.0f, height / 2.0f, -depth / 2.0f,
						0.0f, 0.0f,
						0.0f, 0.0f, -1.0f,

						-width / 2.0f, height / 2.0f, -depth / 2.0f,
						1.0f, 0.0f,
						0.0f, 0.0f, -1.0f,

						-width / 2.0f, -height / 2.0f, -depth / 2.0f,
						1.0f, 1.0f,
						0.0f, 0.0f, -1.0f,

						width / 2.0f, -height / 2.0f, -depth / 2.0f,
						0.0f, 1.0f,
						0.0f, 0.0f, -1.0f,

						//Left
						-width / 2.0f, height / 2.0f, -depth / 2.0f,
						0.0f, 0.0f,
						-1.0f, 0.0f, 0.0f,

						-width / 2.0f, height / 2.0f, depth / 2.0f,
						1.0f, 0.0f,
						-1.0f, 0.0f, 0.0f,

						-width / 2.0f, -height / 2.0f, depth / 2.0f,
						1.0f, 1.0f,
						-1.0f, 0.0f, 0.0f,

						-width / 2.0f, -height / 2.0f, -depth / 2.0f,
						0.0f, 1.0f,
						-1.0f, 0.0f, 0.0f,

						//Top
						-width / 2.0f, height / 2.0f, -depth / 2.0f,
						0.0f, 0.0f,
						0.0f, 1.0f, 0.0f,

						width / 2.0f, height / 2.0f, -depth / 2.0f,
						1.0f, 0.0f,
						0.0f, 1.0f, 0.0f,

						width / 2.0f, height / 2.0f, depth / 2.0f,
						1.0f, 1.0f,
						0.0f, 1.0f, 0.0f,

						-width / 2.0f, height / 2.0f, depth / 2.0f,
						0.0f, 1.0f,
						0.0f, 1.0f, 0.0f,

						//Back
						-width / 2.0f, -height / 2.0f, depth / 2.0f,
						0.0f, 0.0f,
						0.0f, -1.0f, 0.0f,

						width / 2.0f, -height / 2.0f, depth / 2.0f,
						1.0f, 0.0f,
						0.0f, -1.0f, 0.0f,

						width / 2.0f, -height / 2.0f, -depth / 2.0f,
						1.0f, 1.0f,
						0.0f, -1.0f, 00f,

						-width / 2.0f, -height / 2.0f, -depth / 2.0f,
						0.0f, 1.0f,
						0.0f, -1.0f, 0.0f,
				}),
				new int[]{
						0, 1, 2, 2, 3, 0, //Front
						4, 5, 6, 6, 7, 4, //Left
						8, 9, 10, 10, 11, 8, //Back
						12, 13, 14, 14, 15, 12, //Right
						16, 17, 18, 18, 19, 16, //Top
						20, 21, 22, 22, 23, 20 //Bottom
				}
		);
	}

	public static GeometryData sphere(int slices, int stacks) {
		var sphere = ParShapes.par_shapes_create_parametric_sphere(slices, stacks);
		if (sphere == null) {
			throw new IllegalStateException("Failed to create Sphere mesh");
		}

		var cnt = sphere.npoints();

		var pPositions = sphere.points(cnt * 3);
		var positions = new Vector3f[cnt];
		for (var i = 0; i < cnt; i++) {
			positions[i] = new Vector3f(
					pPositions.get(),
					pPositions.get(),
					pPositions.get()
			);
		}

		var pTexCoords = sphere.tcoords(cnt * 2);
		var texCoords = new Vector2f[cnt];
		if (pTexCoords != null) {
			for (var i = 0; i < cnt; i++) {
				texCoords[i] = new Vector2f(
						pTexCoords.get(),
						pTexCoords.get()
				);
			}
		}

		var pNormals = sphere.normals(cnt * 3);
		var normals = new Vector3f[cnt];
		if (pNormals != null) {
			for (var i = 0; i < cnt; i++) {
				normals[i] = new Vector3f(
						pNormals.get(),
						pNormals.get(),
						pNormals.get()
				);
			}
		}

		var vertices = new Vertex[cnt];
		for (var i = 0; i < cnt; i++) {
			vertices[i] = new Vertex(
					positions[i],
					texCoords[i],
					normals[i]
			);
		}

		var pTriangles = sphere.triangles(cnt * 6);
		var indices = new int[cnt * 6];
		for (var i = 0; i < indices.length; i++) {
			indices[i] = pTriangles.get();
		}

		return new GeometryData(
				vertices,
				indices
		);
	}

}