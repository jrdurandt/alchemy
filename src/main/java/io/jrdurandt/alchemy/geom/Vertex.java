package io.jrdurandt.alchemy.geom;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public record Vertex(Vector3f position, Vector2f texCoord, Vector3f normal) {
	public static final int POSITION_INDEX = 0;
	public static final int TEX_COORD_INDEX = 1;
	public static final int NORMAL_INDEX = 2;

	public static final int STRIDE = Vertex.SIZE * Float.BYTES;
	public static final int POSITION_SIZE = 3;
	public static final int TEX_COORD_SIZE = 2;
	public static final int NORMAL_SIZE = 3;
	public static final int SIZE = POSITION_SIZE + TEX_COORD_SIZE + NORMAL_SIZE;

	public static final int POSITION_OFFSET = 0;
	public static final int TEX_COORD_OFFSET = POSITION_OFFSET + POSITION_SIZE;
	public static final int NORMAL_OFFSET = TEX_COORD_OFFSET + TEX_COORD_SIZE;

	public float[] array() {
		return new float[]{
				this.position.x, this.position.y, this.position.z,
				this.texCoord.x, this.texCoord.y,
				this.normal.x, this.normal.y, this.normal.z
		};
	}

	public static Vertex[] build(float[] vertexData) {
		var vertexCount = vertexData.length / SIZE;
		var vertices = new Vertex[vertexCount];
		for (int i = 0, v = 0; i < vertexCount; i ++, v += SIZE) {
			vertices[i] = new Vertex(
					new Vector3f(vertexData[v], vertexData[v + 1], vertexData[v + 2]),
					new Vector2f(vertexData[v + 3], vertexData[v + 4]),
					new Vector3f(vertexData[v + 5], vertexData[v + 6], vertexData[v + 7])
			);
		}
		return vertices;
	}

	public static void enableVertexAttribs(){
		var stride = SIZE * Float.BYTES;

		GL20.glEnableVertexAttribArray(POSITION_INDEX);
		GL20.glVertexAttribPointer(
				POSITION_INDEX,
				POSITION_SIZE,
				GL11.GL_FLOAT,
				false,
				stride,
				(long) POSITION_OFFSET * Float.BYTES
		);

		GL20.glEnableVertexAttribArray(TEX_COORD_INDEX);
		GL20.glVertexAttribPointer(
				TEX_COORD_INDEX,
				TEX_COORD_SIZE,
				GL11.GL_FLOAT,
				false,
				stride,
				(long) TEX_COORD_OFFSET * Float.BYTES
		);

		GL20.glEnableVertexAttribArray(NORMAL_INDEX);
		GL20.glVertexAttribPointer(
				NORMAL_INDEX,
				NORMAL_SIZE,
				GL11.GL_FLOAT,
				false,
				stride,
				(long) NORMAL_OFFSET * Float.BYTES
		);
	}

	public static float[] flatten(Vertex[] vertices) {
		var out = new float[vertices.length * SIZE];
		var offset = 0;
		for (var vertex : vertices) {
			var array = vertex.array();
			System.arraycopy(array, 0, out, offset, array.length);
			offset += array.length;
		}
		return out;
	}
}
